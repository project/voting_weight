<?php

/**
 * @file
 * This file contains declartion for admin form.
 */

/**
 * Menu callback to configure module settings.
 */
function voting_weight_settings() {
  // Weight on roles.
  $roles = voting_weight_user_roles();
  $form['voting_weight_roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Voting weight on roles'),
    '#description' => 'Only integer numbers(1 ~ 999) are allowed. <br /> Users with multi roles, the highest weight would be chosen.',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  foreach ($roles as $rid => $role) {
    $form['voting_weight_roles']['voting_weight_role_'.$rid] = array(
      '#title' => 'Weight for ' . $role,
      '#type' => 'textfield',
      '#default_value' => variable_get('voting_weight_role_'.$rid, 1),
    );
  }
  
  // Weight on entity bundles.
  $entity_types = db_select('votingapi_cache')
    ->fields('votingapi_cache', array('entity_type'))
    ->distinct()
    ->execute()
    ->fetchAllKeyed(0, 0);
  $form['voting_weight_entity_bundles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Voting weight on entity bundles'),
    '#description' => 'Only integer numbers(1 ~ 999) are allowed.',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Only voted entities will be listed here.'),
  );
  foreach ($entity_types as $entity_type) {
    $entity_info = entity_get_info($entity_type);
    $bundle_options = array();
    foreach ($entity_info['bundles'] as $key => $value) {
      $bundle_options[$key] = $value['label'];
    }
    $form['voting_weight_entity_bundles']['voting_weight_entity_bundle_'.$entity_type] = array(
      '#type' => 'checkboxes',
      '#title' => t('Enable weight on !type', array('!type' => $entity_info['label'])),
      '#options' => $bundle_options,
      '#default_value' => variable_get('voting_weight_entity_bundle_'.$entity_type, array()),
    );
  }

  return system_settings_form($form);
}
